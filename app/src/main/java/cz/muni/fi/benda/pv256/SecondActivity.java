package cz.muni.fi.benda.pv256;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Random;

public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        if (savedInstanceState == null) {
            MyFragment myFragment = new MyFragment();
            myFragment.setArguments(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .add(R.id.container, myFragment)
                    .commit();
        }
    }

    public static class MyFragment extends Fragment {
        private static ArrayList<Integer> colorsPool = new ArrayList<Integer>();
        static {
            colorsPool.add(R.color.aqua);
            colorsPool.add(R.color.blue);
            colorsPool.add(R.color.fuchsia);
            colorsPool.add(R.color.gray);
            colorsPool.add(R.color.green);
            colorsPool.add(R.color.lime);
            colorsPool.add(R.color.maroon);
            colorsPool.add(R.color.navy);
            colorsPool.add(R.color.olive);
            colorsPool.add(R.color.purple);
            colorsPool.add(R.color.red);
            colorsPool.add(R.color.silver);
            colorsPool.add(R.color.teal);
            colorsPool.add(R.color.yellow);
        }

        public MyFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            String text = getArguments().getString(MainActivity._EXTRA_KEY);
            View rootView = inflater.inflate(R.layout.fragment, container, false);
            EditText et = (EditText)rootView.findViewById(R.id.editText_frag);
            et.setText(text);

            final Button button = (Button) rootView.findViewById(R.id.button_frag);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Random r = new Random();
                    int color = r.nextInt(colorsPool.size());
                    button.setBackgroundColor(getResources().getColor(colorsPool.get(color)));
                }
            });
            return rootView;
        }
    }
}
